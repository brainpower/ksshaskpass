# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-05 02:51+0000\n"
"PO-Revision-Date: 2022-12-31 15:23+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#: main.cpp:244 main.cpp:313 main.cpp:336
#, kde-format
msgid "Ksshaskpass"
msgstr "Ksshaskpass"

#: main.cpp:246
#, kde-format
msgid "KDE version of ssh-askpass"
msgstr "KDE version of ssh-askpass"

#: main.cpp:248
#, kde-format
msgid ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"
msgstr ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"

#: main.cpp:249
#, kde-format
msgid ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"
msgstr ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"

#: main.cpp:253
#, kde-format
msgid "Armin Berres"
msgstr "Armin Berres"

#: main.cpp:253
#, kde-format
msgid "Current author"
msgstr "Current author"

#: main.cpp:254
#, kde-format
msgid "Hans van Leeuwen"
msgstr "Hans van Leeuwen"

#: main.cpp:254
#, kde-format
msgid "Original author"
msgstr "Original author"

#: main.cpp:255
#, kde-format
msgid "Pali Rohár"
msgstr "Pali Rohár"

#: main.cpp:255
#, kde-format
msgid "Contributor"
msgstr "Contributor"

#: main.cpp:260
#, kde-format
msgctxt "Name of a prompt for a password"
msgid "Prompt"
msgstr "Prompt"

#: main.cpp:266
#, kde-format
msgid "Please enter passphrase"
msgstr "Please enter passphrase"

#: main.cpp:314
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Accept"
